package dev.akii.statusupdate;

import org.bukkit.configuration.file.FileConfiguration;
import org.jetbrains.annotations.NotNull;

public class Utils {

  public static void initConfig(@NotNull StatusUpdate plugin) {
    FileConfiguration config = plugin.getConfig();

    config.addDefault("discord.token", "");
    config.addDefault("discord.prefix", "su!");
    config.addDefault("discord.guildID", "850929171789250560");
    config.addDefault("discord.channelID", "853341163628789761");

    config.addDefault("shutdownMessage", "<:offline:917099160421933146> **Shutting down...**");
    config.addDefault("bootUpMessage", "<:rebooting:917099160400969819> **Booting up...**");
    config.addDefault("onlineMessage", "<:online:917099160417730630> **Server online!**");

    config.options().copyDefaults(true);
    plugin.saveConfig();
  }

}
