package dev.akii.statusupdate;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import org.bukkit.plugin.java.JavaPlugin;

public final class StatusUpdate extends JavaPlugin {

  private JDA api;

  @Override
  public void onEnable() {
    this.getLogger().info("Plugin starting!");

    // Copy all the default config values if they aren't already there
    Utils.initConfig(this);

    String guildID = this.getGuildID();
    String channelID = this.getChannelID();

    String bootUpMessage = this.getConfig().getString("bootUpMessage");
    if(bootUpMessage == null || bootUpMessage.length() == 0){
      throw new RuntimeException("Config value bootUpMessage is empty or missing!");
    }

    UpdateBot bot = new UpdateBot(this);
    this.api = bot.getAPI();

    Guild guild = this.api.getGuildById(guildID);
    if(guild == null) {
      throw new RuntimeException("Provided guild ID was invalid!");
    }
    TextChannel channel = guild.getTextChannelById(channelID);
    if(channel == null) {
      throw new RuntimeException("Provided channel ID was invalid!");
    }

    this.getLogger().info("Publishing startup...");

    channel.sendMessage(bootUpMessage).queue();

    // get the config value before the delayed task because we can't reference the plugin object from inside that scope
    String onlineMessage = this.getConfig().getString("onlineMessage");
    if(onlineMessage == null || onlineMessage.length() == 0) {
      throw new RuntimeException("Config value onlineMessage is empty or missing!");
    }

    // This code runs after the server has started up completely.
    getServer().getScheduler().scheduleSyncDelayedTask(this, () -> channel.sendMessage(onlineMessage).queue());

  }

  @Override
  public void onDisable() {
    this.getLogger().info("Publishing shutdown...");

    Guild guild = this.api.getGuildById(this.getGuildID());
    if(guild == null) {
      throw new RuntimeException("Provided guild ID was invalid!");
    }
    TextChannel channel = guild.getTextChannelById(this.getChannelID());
    if(channel == null) {
      throw new RuntimeException("Provided channel ID was invalid!");
    }

    String shutdownMessage = this.getConfig().getString("shutdownMessage");
    if(shutdownMessage == null || shutdownMessage.length() == 0){
      throw new RuntimeException("Config value shutdownMessage is empty or missing!");
    }

    channel.sendMessage(shutdownMessage).queue();

    this.getLogger().info("Done. Bye!");

  }

  private String getGuildID() {
    String guildID = this.getConfig().getString("discord.guildID");
    if(guildID == null || guildID.length() == 0) {
      throw new RuntimeException("Config value discord.guildID is empty or missing!");
    }

    return guildID;
  }

  private String getChannelID() {
    String channelID = this.getConfig().getString("discord.channelID");
    if(channelID == null || channelID.length() == 0) {
      throw new RuntimeException("Config value discord.channelID is empty or missing!");
    }

    return channelID;
  }
}
