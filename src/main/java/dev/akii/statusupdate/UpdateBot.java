package dev.akii.statusupdate;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.jetbrains.annotations.NotNull;

public class UpdateBot {

  private final StatusUpdate plugin;
  private JDA api;

  public UpdateBot(@NotNull StatusUpdate plugin) {
    this.plugin = plugin;

    String token = plugin.getConfig().getString("discord.token");

    try {

      this.api = JDABuilder.createDefault(token,
                      GatewayIntent.GUILD_MESSAGES)
              .build().awaitReady();

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public StatusUpdate getPlugin() {
    return this.plugin;
  }

  public JDA getAPI() {
    return this.api;
  }

}
